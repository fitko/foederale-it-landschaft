# Dokumentation der föderalen IT-Landschaft

Aufbauend auf den föderalen IT-Architekturrichtlinien hat das föderale IT-Architekturboard die aktuelle IT-Landschaft auf der föderalen Ebene beschrieben. Die IT-Landschaft wurde zum einen als Poster aufbereitet, zum anderen in einem umfangreichen Begleitdokument beschrieben.

Die Dokumentation der IT-Landschaft ist, neben den Architekturrichtlinien, ein weiterer wichtiger Baustein auf dem Weg zur Etablierung eines föderalen IT-Architekturmanagements.

In diesem Repository finden sich die Quelldateien des Poster zur föderalen IT-Landschaft.

Weiterführende Dokumentation und das Poster der föd. IT-Landschaft im PDF-Format finden sich [auf der Webseite der FITKO](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard).